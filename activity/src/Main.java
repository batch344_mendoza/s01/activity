import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubj;
        double secondSubj;
        double thirdSubj;

        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = myObj.nextLine();
        System.out.println("Last Name:");
        lastName = myObj.nextLine();
        System.out.println("First Subject Grade:");
        firstSubj = myObj.nextDouble();
        System.out.println("Second Subject Grade:");
        secondSubj = myObj.nextDouble();
        System.out.println("Third Subject Grade:");
        thirdSubj = myObj.nextDouble();

        double average = (firstSubj + secondSubj + thirdSubj) / 3;

        System.out.println("Good day, "+ firstName + " " + lastName + ".");
        System.out.println("Your grade average is: "+ average);



    }
}